package org.jzl.calculcamerademo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.system.android.commercial.event.OnHomeKeyPressed;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import base.async.Async;
import base.log.LogFilterDef;
import base.log.LogUtil;
import base.utils.BitmapHelper;
import base.utils.ResourceUtil;
import base.utils.StringUtil;
import base.utils.ToastUtil;
import multi.function.scientific.math.calculate.calculator.app.activity.EquationResultActivity;
import multi.function.scientific.math.calculate.calculator.app.ad.AdvertisementSwitcher;
import multi.function.scientific.math.calculate.calculator.app.ad.InterstitialManager;
import multi.function.scientific.math.calculate.calculator.app.app.ApplicationEx;
import multi.function.scientific.math.calculate.calculator.app.callback.OnResultCallBack;
import multi.function.scientific.math.calculate.calculator.app.custom.UploadPhotosDialog;
import multi.function.scientific.math.calculate.calculator.app.manager.EquationManager;
import multi.function.scientific.math.calculate.calculator.app.manager.ServerConfigManager;
import multi.function.scientific.math.calculate.calculator.app.manager.UserPermissionManager;
import multi.function.scientific.math.calculate.calculator.app.model.pojo.DetectionResult;
import multi.function.scientific.math.calculate.calculator.app.util.CameraUtil;
import multi.function.scientific.math.calculate.calculator.app.util.FunctionUtil;
import multi.function.scientific.math.calculate.calculator.app.util.HttpUtil;
import multi.function.scientific.math.calculate.calculator.app.util.StatisticsUtil;
import multi.function.scientific.math.calculate.calculator.app.view.CameraPreview;
import multi.function.scientific.math.calculate.calculator.app.view.cropcontrol.CropController;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by ytq on 2018/2/2.
 */
public class MainCameraPage extends MainBasePage {
    private String TAG = LogFilterDef.EQUATION_EVALUATE;
    private Camera mCamera;
    private CameraPreview mPreview;
    private DetectionResult mDetectionResult;
    private AtomicBoolean mIsProgress = new AtomicBoolean(false);
    private AtomicBoolean mIsVisiable = new AtomicBoolean(false);
    //    private ImageView mToolsPhoto;
    private boolean mMisidentification;
    private CropController mCropController;
    private boolean mIsSatisticsVisit = false;//首次进入做点击事件 统计
    private Bitmap mUploadPicture;
    private boolean mIsTakePhoto = false;//出现耗时拍照时 如果退出main activity 则不跳转结果页

    public MainCameraPage(Activity activity, int res, boolean delayInit) {
        super(activity, res, delayInit);
    }

    @Override
    protected void doInit() {
        init();
        bindAction();
    }

    @Override
    protected boolean onBackPressed() {
        return super.onBackPressed();
    }

    public void init() {
        mCropController = new CropController(findViewById(RelativeLayout.class, R.id.layout_crop_control), new CropController.TouchStateListener() {
            @Override
            public void onDragBegan() {
            }

            @Override
            public void onDragEnded() {
            }
        });
    }

    private void bindAction() {
        findViewById(R.id.iv_take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsSatisticsVisit) {
                    mIsSatisticsVisit = true;
                    StatisticsUtil.logParamsEventForce(StatisticsUtil.E_PAGE_VIEW, StatisticsUtil.P_PAGE_VISIBLE, "camera page");
                }
                mIsTakePhoto = true;
                if (mCamera == null) return;
                mIsProgress.set(true);
                findViewById(TextView.class, R.id.tv_crop_status).setText("");
                findViewById(ImageView.class, R.id.iv_take_photo).setImageResource(R.drawable.ic_photo_shutter);
                findViewById(R.id.iv_progress_bar).setVisibility(View.VISIBLE);
                try {
                    mCamera.takePicture(null, null, new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(final byte[] data, Camera camera) {
                            try {
                                //stopPreview();
                                if (((MainActivity) mContext).isFinishing()) {
                                    return;
                                }
                                Bitmap bm = BitmapHelper.toBitmap(data);

                                if (bm.getWidth() > bm.getHeight()) {
                                    bm = BitmapHelper.rotate(bm, 90);
                                }
                                if (BuildConfig.DEBUG) {
                                    LogUtil.d(TAG, "got bitmap size = " + bm.getWidth() + ", " + bm.getHeight());
                                }
                                RelativeLayout layout = findViewById(RelativeLayout.class, R.id.layout_crop_control);
                                Rect cropFrame = new Rect(layout.getLeft(), layout.getTop(), layout.getRight(), layout.getBottom());

                                int inset = (int) ResourceUtil.getDimensionDp(R.dimen.crop_corner_width_halved);
                                int viewWidth = getView().getWidth();
                                int viewHeight = getView().getHeight();
                                int cropWidth = cropFrame.width() - inset * 2;
                                //                        int cropHeight = cropFrame.height() - inset * 2;
                                int cropHeight = cropFrame.height() + inset / 2;
                                int centerX = bm.getWidth() / 2;
                                int centerY = bm.getHeight() / 2;
                                int targetWidth = (cropWidth * bm.getWidth()) / viewWidth;
                                int targetHeight = (cropHeight * bm.getHeight()) / viewHeight;

                                if (BuildConfig.DEBUG) {
                                    LogUtil.d(TAG, "screen size = " + viewWidth + ", " + viewHeight);
                                    LogUtil.d(TAG, "target size = " + targetWidth + ", " + targetHeight);
                                }
                                mUploadPicture = Bitmap.createBitmap(bm, centerX - targetWidth / 2, centerY - targetHeight / 2, targetWidth, targetHeight);
                                mUploadPicture = Bitmap.createScaledBitmap(mUploadPicture, targetWidth / 3, targetHeight / 3, false);
                                uploadImage(mUploadPicture);
                            } catch (Exception e) {
                                mMisidentification = true;
                                ToastUtil.showToast(ResourceUtil.getString(R.string.take_photo_error_tips), 0);
                            }
                        }
                    });
                } catch (Exception e) {
                    ToastUtil.showToast(ResourceUtil.getString(R.string.take_photo_error_tips), 0);
                    StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "take photo failed");
                }

                findViewById(R.id.iv_take_photo).setEnabled(false);

            }
        });

    }

    private void stopPreview() {
        if (mCamera != null) {
            try {
                mCamera.setPreviewCallback(null);
                mPreview.getHolder().removeCallback(mPreview);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onVisibleChanged(final boolean visible) {
        mIsVisiable.set(visible);
        if (mIsProgress.get()) {
            return;
        }
        if (BuildConfig.DEBUG) {
            LogUtil.d(TAG, "visible = " + visible);
        }

        if (visible) {
            if (!UserPermissionManager.hasPermission(mContext, new String[]{Manifest.permission.CAMERA})) {
                pageInVisible();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (BuildConfig.DEBUG) LogUtil.d(UserPermissionManager.TAG, "权限集合为空");
                    if (UserPermissionManager.cameraPermissionDialog != null && UserPermissionManager.cameraPermissionDialog.isShowing()) {
                        return;
                    }
                    UserPermissionManager.requestMultiPermissions((MainActivity) mContext, UserPermissionManager.CAMERA_PERMS, (MainActivity) mContext, UserPermissionManager.CODE_MULTI_PERMISSION);
                }
            } else {
                startPreview();
//            Async.scheduleTaskOnUiThread(500, new Runnable() {
//                @Override
//                public void run() {
//                    if (visible) {
//                        startPreview();
//                    } else {
//                        pageInVisible();
//                    }
//                }
//            });
            }
        } else {
            pageInVisible();
        }
    }

    private void pageInVisible() {
        findViewById(ImageView.class, R.id.iv_camera_snapshot).setImageBitmap(null);
        findViewById(ImageView.class, R.id.iv_camera_snapshot).setBackgroundColor(ResourceUtil.getColor(R.color.black));
        findViewById(R.id.iv_progress_bar).setVisibility(View.GONE);
        findViewById(TextView.class, R.id.tv_crop_status).setText("");
    }


    @Override
    public void pageOnResume() {
        super.pageOnResume();
//        startPreview();
    }

    @Override
    public void pageOnDestroy() {
        super.pageOnDestroy();
        stopPreview();
    }

    private void startPreview() {
        findViewById(ImageView.class, R.id.iv_camera_snapshot).setBackgroundColor(ResourceUtil.getColor(R.color.color_transparent));
        findViewById(ImageView.class, R.id.iv_preview).setImageBitmap(null);
        findViewById(ImageView.class, R.id.iv_camera_snapshot).setImageBitmap(null);
        resetDragControl();

        if (mCamera != null) {
            try {
                mCamera.startPreview();
                return;
            } catch (Exception e) {
                LogUtil.error(e);
                //jump to below
            }
        }

        try {
            mCamera = CameraUtil.getCameraInstance(mContext);

            if (mCamera == null) {
//                showAlert("Can not connect to camera.");
            } else {
                mPreview = new CameraPreview(mContext, mCamera);
                findViewById(FrameLayout.class, R.id.layout_camera_preview).removeAllViews();
                findViewById(FrameLayout.class, R.id.layout_camera_preview).addView(mPreview);
            }

        } catch (Exception e) {
            LogUtil.error(e);
        }
    }

    private void showErrorAndReset(String errMessage) {
        startPreview();
        resetDragControl();
    }

    public void resetDragControl() {
        findViewById(ImageView.class, R.id.iv_preview).setImageBitmap(null);
        ViewGroup.LayoutParams layoutParams = findViewById(RelativeLayout.class, R.id.layout_crop_control).getLayoutParams();
        layoutParams.width = (int) ResourceUtil.getDimensionDp(R.dimen.crop_control_width);
        layoutParams.height = (int) ResourceUtil.getDimensionDp(R.dimen.crop_control_height);
        findViewById(RelativeLayout.class, R.id.layout_crop_control).setLayoutParams(layoutParams);
        findViewById(R.id.iv_take_photo).setEnabled(true);
    }

    private void showUploadResponse(final String errorMessage) {
        Async.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (StringUtil.isEmpty(errorMessage)) {
                        if (mDetectionResult != null && !StringUtil.isEmpty(mDetectionResult.wolfram)) {
                            //preload interstitial
                            long interstitialLoadTime = 0;
                            if (ServerConfigManager.getInstance().isInterstitialAdEnable()) {
                                InterstitialManager.getInstance().loadAd(ApplicationEx.getInstance(), AdvertisementSwitcher.SERVER_KEY_INTERSTITIAL_RESULT, "");
                                interstitialLoadTime = System.currentTimeMillis();
                            }

                            String wolfram = mDetectionResult.wolfram;
                            String[] array = wolfram.split("\\}\\,\\{");
                            ArrayList<String> list = new ArrayList<>();
                            for (int i = 0; i < array.length; i++) {
                                String exp = array[i].replaceAll(",", "");
                                String arrayStr = EquationManager.getInstance().getServerExpression(exp);
                                list.add(arrayStr);
                            }
                            if (BuildConfig.DEBUG) {
                                LogUtil.d(LogFilterDef.EQUATION_EVALUATE, "createExpression = " + list.toString());
                            }
                            LinkedHashMap<String, String> returnMap = EquationManager.getInstance().createExpression(list);
                            //String returnStr = EquationManager.getInstance().createExpression(mDetectionResult.latex_list);

                            final long finalInterstitialLoadTime = interstitialLoadTime;
                            EquationManager.getInstance().evaluate(returnMap, new OnResultCallBack() {

                                @Override
                                public void success(final Object[] obj) {
                                    if (BuildConfig.DEBUG) {
                                        LogUtil.d(LogFilterDef.EQUATION_EVALUATE, "evaluate success = " + obj[0]);
                                    }
                                    Async.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            findViewById(R.id.iv_progress_bar).setVisibility(View.GONE);
                                            String expression = mDetectionResult.mathml;
                                            if (StringUtil.isEmpty(expression)) {
                                                expression = mDetectionResult.latex_list.toString();
                                            }

                                            final String finalExpression = expression;
                                            Async.scheduleTaskOnUiThread(0, new Runnable() {
                                                @Override
                                                public void run() {
                                                    boolean canShowTop = obj.length > 1 ? (boolean) obj[1] : true;
                                                    showResult(finalExpression.toString(), (String) obj[0], false, canShowTop);
                                                }
                                            });
                                        }
                                    });
                                }

                                @Override
                                public void fail(final Object obj) {
                                    Async.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            findViewById(R.id.iv_progress_bar).setVisibility(View.GONE);
                                            String expression = mDetectionResult.mathml;
                                            if (StringUtil.isEmpty(expression)) {
                                                expression = mDetectionResult.latex_list.toString();
                                            }
                                            showResult(expression, (String) obj, true, true);
                                        }
                                    });
                                }
                            });
                        } else {
                            showUploadResponse(ResourceUtil.getString(R.string.photo_identity_failed));
                            StatisticsUtil.logEventForce(StatisticsUtil.PHOTO_IDENTUTY_FAILED);
                        }

                    } else {
                        mIsProgress.set(false);
                        Async.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(ImageView.class, R.id.iv_take_photo).setImageResource(R.drawable.ic_photo_shutter_error);
                                findViewById(R.id.iv_progress_bar).setVisibility(View.GONE);
                                findViewById(TextView.class, R.id.tv_crop_status).setText(errorMessage);
                                showErrorAndReset(errorMessage);
                            }
                        });
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        LogUtil.error(e);
                    }
                    showUploadResponse(ResourceUtil.getString(R.string.photo_identity_failed));
                    StatisticsUtil.logEventForce(StatisticsUtil.PHOTO_IDENTUTY_FAILED);
                }
            }
        });
    }

    private void showResult(String subjectExpression, String resultExpression, boolean isError, boolean canShowTop) {
        mIsProgress.set(false);
        if (!mIsTakePhoto) {
            return;
        }

        Intent intent = new Intent(ApplicationEx.getInstance(), EquationResultActivity.class);
        intent.putExtra(EquationResultActivity.INTENT_DATA_SUBJECT, subjectExpression);
        intent.putExtra(EquationResultActivity.INTENT_DATA_SUBJECT_MATH, subjectExpression);
        intent.putExtra(EquationResultActivity.INTENT_DATA_RESULT, resultExpression);
        intent.putExtra(EquationResultActivity.INTENT_DATA_IS_ERROR, isError);
        intent.putExtra(EquationResultActivity.INTENT_DATA_SHOW_TOP, canShowTop);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ApplicationEx.getInstance().startActivity(intent);
        if (mContext != null) {
            ((Activity) mContext).overridePendingTransition(0, 0);
        }
    }


    //-----------Recognize Equation From Photo begin--------

    /**
     * others key -> self key
     *
     * @param bitmap
     */
    private void uploadImage(Bitmap bitmap) {
        findViewById(R.id.iv_progress_bar).setVisibility(View.VISIBLE);
        recognizeEquationWithOthersKey(bitmap);

        if (mMisidentification) {
            UploadPhotosDialog.getInstance(ApplicationEx.getInstance()).showDialog(mUploadPicture);
        }

    }

    private void recognizeEquationWithOthersKey(final Bitmap bitmap) {
        String appId = FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("KKCWCbvteKRI1IJbO7jeYw==", String.class));
        String appKey = FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("NoKxdxlhVXXeiqYcpRd2qg==", String.class));
        HttpUtil.upLoadEquationImage(appId, appKey, bitmap, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                showUploadResponse(ResourceUtil.getString(R.string.network_error));
                if (BuildConfig.DEBUG) {
                    LogUtil.d(TAG, "uploadImage with other key response failed: " + e.getMessage());
                }

                StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "network error other");
                StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_OTHERS, "network error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    if (BuildConfig.DEBUG) {
                        LogUtil.d(TAG, "uploadImage with other key response: " + responseString);
                    }
                    mDetectionResult = new Gson().fromJson(responseString, DetectionResult.class);
                    if ((mDetectionResult == null || StringUtil.isEmpty(mDetectionResult.wolfram)) &&
                            !StringUtil.isEmpty(FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("xn3v1huuCeLAQ8YUziaCIae5L03d5GR4cVprwrWvMsE=", String.class))) &&
                            !(FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("NoKxdxlhVXXeiqYcpRd2qg==", String.class)).equals(FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("xn3v1huuCeLAQ8YUziaCIae5L03d5GR4cVprwrWvMsE=", String.class))))) {
                        //try load with self key
                        recognizeEquationWithSelfKey(bitmap);
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "server recognize failed other");
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_OTHERS, "recognize failed");
                    } else {
                        showUploadResponse(null);

                        if (mDetectionResult == null || StringUtil.isEmpty(mDetectionResult.wolfram)) {
                            StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "server recognize failed other");
                            StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_OTHERS, "recognize failed");
                        } else {
                            StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "server recognize Success other");
                            StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_OTHERS, "recognize success");
                        }
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        LogUtil.error(e);
                    }
                    showUploadResponse(ResourceUtil.getString(R.string.photo_identity_failed));
                    StatisticsUtil.logEventForce(StatisticsUtil.PHOTO_IDENTUTY_FAILED);
                }
            }
        });

        if (BuildConfig.DEBUG) {
            LogUtil.d(TAG, "uploadImage with other: <" + appId + ", " + appKey + ">");
        }
    }

    private void recognizeEquationWithSelfKey(Bitmap bitmap) {
        String appId = FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("xn3v1huuCeLAQ8YUziaCIYoN4VYzEdPZxdVWlxbCyt0=", String.class));
        String appKey = FunctionUtil.decrypt(ServerConfigManager.getInstance().getServerConfig("xn3v1huuCeLAQ8YUziaCIae5L03d5GR4cVprwrWvMsE=", String.class));
        HttpUtil.upLoadEquationImage(appId, appKey, bitmap, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                showUploadResponse(ResourceUtil.getString(R.string.network_error));
                if (BuildConfig.DEBUG) {
                    LogUtil.d(TAG, "uploadImage with self key response: " + e.getMessage());
                }

                StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "network error self");
                StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_SELF, "network error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    if (BuildConfig.DEBUG) {
                        LogUtil.d(TAG, "uploadImage with self key response: " + responseString);
                    }
                    mDetectionResult = new Gson().fromJson(responseString, DetectionResult.class);
                    showUploadResponse(null);

                    if (mDetectionResult == null || StringUtil.isEmpty(mDetectionResult.wolfram)) {
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "server recognize failed self");
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_SELF, "recognize failed");
                    } else {
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_EQUATION, StatisticsUtil.P_EQUATION_CAMERA, "server recognize Success self");
                        StatisticsUtil.logParamsEventForce(StatisticsUtil.E_MATH_API_CALL, StatisticsUtil.P_MATH_API_SELF, "recognize success");
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        LogUtil.error(e);
                    }

                    showUploadResponse(ResourceUtil.getString(R.string.photo_identity_failed));
                    StatisticsUtil.logEventForce(StatisticsUtil.PHOTO_IDENTUTY_FAILED);
                }
            }
        });

        if (BuildConfig.DEBUG) {
            LogUtil.d(TAG, "uploadImage with self: <" + appId + ", " + appKey + ">");
        }
    }

    //-----------Recognize Equation From Photo end--------

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMain(OnHomeKeyPressed event) {
        if (!event.longPress) {
            //pageInVisible();
            stopPreview();
        }
    }


}
